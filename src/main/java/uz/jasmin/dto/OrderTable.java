package uz.jasmin.dto;

public class OrderTable {
    private Long id;
    private String ordered;
    private String ready;

    public OrderTable(String ordered) {
        this.ordered = ordered;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrdered() {
        return ordered;
    }

    public void setOrdered(String ordered) {
        this.ordered = ordered;
    }

    public String getReady() {
        return ready;
    }

    public void setReady(String ready) {
        this.ready = ready;
    }
}
