package uz.jasmin.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import uz.jasmin.dto.MarketDB;


import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class OrderStateModalController implements Initializable {
    @FXML
    private VBox in_process;
    @FXML
    private VBox ready;
    private Map<Integer,Label> proc;
    private Map<Integer,Label> ready_list;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
            proc = new HashMap<>();
            ready_list = new HashMap<>();
            events();


    }

    public void events(){
        Timeline ten = new Timeline(new KeyFrame(Duration.seconds(2), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                in_process.getChildren().clear();
                ready.getChildren().clear();
                LocalDate ld = LocalDate.now();

                PreparedStatement statement = null;
                PreparedStatement statement1 = null;
                try {
                    statement = MarketDB.getInstance().getConnection().prepareStatement("SELECT * from sales_by_date where `status`=0 and date=?");
                    statement1 = MarketDB.getInstance().getConnection().prepareStatement("SELECT * from sales_by_date where `status`=1 and date=?");
                    statement.setString(1,ld.toString());
                    statement1.setString(1,ld.toString());

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                try {
                    ResultSet rs = statement.executeQuery();
                    ResultSet rs1= statement1.executeQuery();
                    while (rs.next()){
                        Label l = new Label("Заказ №"+rs.getInt(3));
                        l.getStyleClass().add("order_element");
                        in_process.getChildren().add(l);


                    }

                    while (rs1.next()){
                        Label l = new Label("Заказ №"+rs1.getInt(3));
                        l.getStyleClass().add("order_element");
                        ready.getChildren().add(l);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        }));
        ten.setCycleCount(Timeline.INDEFINITE);
        ten.play();

    }

}
