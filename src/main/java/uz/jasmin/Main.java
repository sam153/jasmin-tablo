package uz.jasmin;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception
    {

        AnchorPane root = new AnchorPane();
        Scene scene = new Scene(root);
        int index = 1;
        for (Screen screen : Screen.getScreens()) {
            Rectangle2D bounds = screen.getVisualBounds();

                if(index == 2)
                {
                primaryStage.setX(bounds.getMinX());
                primaryStage.setY(bounds.getMinY());
                }
            index ++;
        }
        root.getChildren().add(FXMLLoader.load(getClass().getClassLoader().getResource("order_state_modal.fxml")));
        primaryStage.setFullScreen(true);
        primaryStage.setFullScreenExitHint("");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
